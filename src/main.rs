use std::fs;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug, PartialEq, Clone)]
struct Robot {
    x: i32,
    y: i32,
    id: i32,
    orientation: Orientation,
    instruction: Vec<Instruction>,
}
#[derive(Debug, PartialEq, Clone)]
struct Robots {
    tableau: Vec<Robot>,
}
#[derive(Debug, PartialEq, Clone)] //pour recuper struc et enum pour les test
enum Orientation {
    Nord,
    Sud,
    Est,
    Ouest,
}

#[derive(Debug, PartialEq, Clone)] //pour recuper struc et enum pour les test
enum Instruction {
    L,
    R,
    F,
}
/// li la valeur et fait un match pour voir avec quel instruction elle correspond en "string"
fn read_char(valeur: &char) -> Instruction {
    match valeur {
        'L' => Instruction::L,
        'R' => Instruction::R,
        'F' => Instruction::F,
        _ => Instruction::L,
    }
}

/// li la valeur et fait un match pour voir avec quel orientation elle correspond en "string" et si erreur de saisie Nord part défaut
fn read_orientation(valeur: &str) -> Orientation {
    match valeur {
        "N" => Orientation::Nord,
        "O" => Orientation::Ouest,
        "S" => Orientation::Sud,
        "E" => Orientation::Est,
        _ => Orientation::Nord,
    }
}

fn deplacement(robots: &mut Robots) {
    for mut rob in &mut robots.tableau {
        for lettre in rob.instruction.iter() {
            match lettre {
                Instruction::L => match rob.orientation {
                    Orientation::Nord => rob.orientation = Orientation::Ouest,
                    Orientation::Sud => rob.orientation = Orientation::Est,
                    Orientation::Est => rob.orientation = Orientation::Nord,
                    Orientation::Ouest => rob.orientation = Orientation::Sud,
                },
                Instruction::R => match rob.orientation {
                    Orientation::Nord => rob.orientation = Orientation::Est,
                    Orientation::Sud => rob.orientation = Orientation::Ouest,
                    Orientation::Est => rob.orientation = Orientation::Sud,
                    Orientation::Ouest => rob.orientation = Orientation::Nord,
                },
                Instruction::F => match rob.orientation {
                    Orientation::Nord => rob.y = rob.y + 1,
                    Orientation::Sud => rob.y = rob.y - 1,
                    Orientation::Est => rob.x = rob.x + 1,
                    Orientation::Ouest => rob.x = rob.x - 1,
                },
            }
        }
    }
}

fn read(f: String, robots: &mut Robots) {
    let mut lines = f.lines();
    let mut world = lines
        .next()
        .expect("Error crash: no world")
        .split_whitespace();
    let x_map = world.next(); // a parser
    let y_map = world.next(); // a parser
    let _y_map = y_map.expect("Error crash: no y map").parse::<i32>();
    let _x_map = x_map.expect("Error crash: no y map").parse::<i32>();
    let mut id = 0;

    let _robot = &robots.tableau;
    // Some robot si assignement réussi
    //si une des deux line.next revoie None stop boucle.
    while let (Some(_robot), Some(instruction)) = (lines.next(), lines.next()) {
        id += 1;
        let mut robot = _robot.split_whitespace();
        let x_pos = robot.next().unwrap();
        let y_pos = robot.next().unwrap();
        let orientation = robot.next().unwrap();
        let instructions: Vec<Instruction> = instruction.chars().map(|i| read_char(&i)).collect();
        let y_pos = y_pos.parse::<i32>().unwrap();
        let x_pos = x_pos.parse::<i32>().unwrap();
        let robot = Robot {
            orientation: read_orientation(&orientation),
            id: id,
            x: x_pos,
            y: y_pos,
            instruction: instructions,
        };
        robots.tableau.push(robot);
    }
}

fn main() {
    // get command line arguments
    let args: Vec<String> = std::env::args().collect();
    // If not enough argument passed it will crash badlyyyy :)
    let world_buff = fs::read_to_string(&args[1]).expect("open failed");
    let rb = Robot {
        x: 1,
        y: 1,
        orientation: Orientation::Nord,
        instruction: [Instruction::F].to_vec(),
        id: 1,
    };
    let mut rbs = Robots {
        tableau: [rb].to_vec(),
    };
    read(world_buff, &mut rbs);
    println!("{:?}", rbs);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_Orientation_south() {
        assert_eq!(read_orientation(&"S"), Orientation::Sud);
    }
    #[test]
    fn test_Orientation_north() {
        assert_eq!(read_orientation(&"N"), Orientation::Nord);
    }
    #[test]
    fn test_Orientation_erreur() {
        assert_eq!(read_orientation(&" "), Orientation::Nord);
    }
}
